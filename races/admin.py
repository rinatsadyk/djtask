from django.contrib import admin
from .models import CarModel, Car, ActiveRace


admin.site.register(CarModel)
admin.site.register(Car)
admin.site.register(ActiveRace)

