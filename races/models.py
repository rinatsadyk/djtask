from django.db import models


class CarModel(models.Model):
    """
    Car model, like: Belaz.
    """
    name = models.CharField(max_length=30, unique=True)
    load_capasity = models.FloatField()

    def __str__(self):
        return self.name


class Car(models.Model):
    """
    Concrete car used.
    """
    car_model = models.ForeignKey(CarModel, on_delete=models.CASCADE)
    id_number = models.CharField(max_length=12, unique=True)
    
    def __str__(self):
        return self.id_number


class ActiveRace(models.Model):
    """
    Some race(round). Exists only during a the concrete race.  
    """
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    ore_weight = models.FloatField(null=True, blank=True) # A car can be empty

    @property
    def overloading(self):
        return 100 * self.ore_weight / self.car.car_model.load_capasity

    def __str__(self):
        return f'{self.car}/{self.ore_weight}'    
