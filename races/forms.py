from django import forms
from .models import CarModel


class ActiveRaceForm(forms.ModelForm):
    car = forms.ModelChoiceField(
        label="Model", queryset=CarModel.objects.all(), required=False, empty_label="Все"
    )
    class Meta:
        model = CarModel
        fields = ['car']
