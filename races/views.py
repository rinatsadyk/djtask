from django.shortcuts import render
from .models import ActiveRace
from .forms import ActiveRaceForm


def race_list(request):
    
    if request.method == 'POST':
        form = ActiveRaceForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['car'] == None:
                races = ActiveRace.objects.all()
            else:
                races = ActiveRace.objects.filter(car__car_model=form.cleaned_data['car'])
            return render(request, 'races/race_list.html', {'races': races, 'form': form})
    else:
        form = ActiveRaceForm

    races = ActiveRace.objects.all()
    return render(request, 'races/race_list.html', {'races': races, 'form': form})

