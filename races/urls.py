from django.urls import path
from . import views


app_name = 'races'

urlpatterns = [
    path('', views.race_list, name='race_list'),
]
